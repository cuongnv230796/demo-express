const express = require('express');
const bodyParser = require('body-parser');
const env = require('dotenv').config();

const PORT = process.env.PORT || 3000;

const app = express();

app.use('/', (req, res, next) => {
    res.send('Hello SG4 Team');
    next();
});

app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}!`);
})
